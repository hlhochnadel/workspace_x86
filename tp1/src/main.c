/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    int timerCount = 0;
    _Bool barreraFlag = 0, timerFlag = 0;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while(input != EXIT){
        input = hw_LeerEntrada();

        if(input == SENSOR_1){
            if(barreraFlag){
                hw_NuevoVehiculo();
                timerCount = 0;
                timerFlag = 0;
            }
            else{
                hw_AbrirBarrera();
                barreraFlag = 1;
            }
        }

        if(input == SENSOR_2 && barreraFlag){
            hw_VehiculoAvanzo();
            timerFlag = 1;
        }
        else{
            if(input == SENSOR_2){
            hw_Buzzer();  
            }
            
        }

        if(timerFlag){
            if(timerCount == 50){
                hw_CerrarBarrera();
                barreraFlag = 0;
                timerFlag = 0;
                timerCount = 0;
            }
            else{
                timerCount++;
            }
        }

        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
