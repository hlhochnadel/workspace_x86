#ifndef _FUNC_H_
#define _FUNC_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define esperandoFicha     0
#define esperandoEleccion  1
#define teElegido          2
#define cafeElegido        3
#define indicadorSonoro    4

#define exit                  27  // ASCII para la tecla Esc 
#define evFichaIngresada      49  // ASCII para la tecla 1 
#define evSeleccionTe         50  // ASCII para la tecla 2 
#define evSeleccionCafe       51  // ASCII para la tecla 3

#define tiempoCafe         45
#define tiempoTe           30
#define tiempoEspera       30
#define tiempoSonoro       2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void func_Init(void);
void func_DeInit(void);
void func_Pausems(uint16_t t);
_Bool func_ev1seg(void);
_Bool func_evToggleLed(void);
void func_esperarFicha(void);
void func_esperarEleccion(void);
void func_tiempoTranscurrido(void);
void func_sirviendoTe(void);
void func_teServido(void);
void func_sirviendoCafe(void);
void func_cafeServido(void);
void func_alarmaOn(void);
void func_alarmaOff(void);
uint8_t func_readInput(void);
_Bool func_toggleLed(_Bool ledStatus);

/*==================[end of file]============================================*/
#endif /* #ifndef _FUNC_H_ */
