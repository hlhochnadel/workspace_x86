/*==================[inclusions]=============================================*/

#include "func.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void func_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void func_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void func_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

_Bool func_ev1seg(void){
    return 1;
}

_Bool func_evToggleLed(void){
    return 1;
}

void func_esperarFicha(void){
    printf("Ingrese una ficha.\n\n");
}

void func_esperarEleccion(void){
    printf("Ficha ingresada. Presione 2 para elegir Te o presione 3 para elegir Cafe.\n\n");
}

void func_tiempoTranscurrido(void){
    printf("Ficha devuelta. Ingrese una ficha nuevamente.\n\n");
}

void func_sirviendoTe(void){
    printf("Sirviendo Te.\n\n");
}

void func_teServido(void){
    printf("Te servido.\n\n");
}

void func_sirviendoCafe(void){
    printf("Sirviendo Cafe.\n\n");
}

void func_cafeServido(void){
    printf("Cafe servido.\n\n");
}

void func_alarmaOn(void){
    printf("Indicador sonoro activado.\n\n");
}

void func_alarmaOff(void){
    printf("Indicador sonoro desactivado.\n\n");
}

uint8_t func_readInput(void){
    return getchar();
}

_Bool func_toggleLed(_Bool ledStatus){
    ledStatus = !ledStatus;
    if(ledStatus){
        printf("Led ON\n\n");
    }
    else{
        printf("Led OFF\n\n");
    }
    return ledStatus;
}
/*==================[end of file]============================================*/
