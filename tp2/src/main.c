/*==================[inclusions]=============================================*/

#include "main.h"
#include "func.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void){
    uint8_t input, status = esperandoFicha;
    int timerCount = 0, msHalfCycle = 500, msTimer = 0;
    _Bool ledStatus = 0, ev1seg = 0, evToggleLed = 0;

    func_Init();
    func_esperarFicha();
    while(input != exit){
        input = func_readInput();
        switch(status){
            case esperandoFicha:
                if(input == evFichaIngresada){
                    status = esperandoEleccion;
                    func_esperarEleccion();
                    timerCount = 0;
                    ledStatus = 0;
                }
                else if(evToggleLed){
                    ledStatus = func_toggleLed(ledStatus);
                }
                break;

            case esperandoEleccion:
                if(input == evSeleccionTe){
                    status = teElegido;
                    func_sirviendoTe();
                    timerCount = 0;
                    msHalfCycle = 100;
                }
                else if(input == evSeleccionCafe){
                    status = cafeElegido;
                    func_sirviendoCafe();
                    timerCount = 0;
                    msHalfCycle = 100;
                }
                else if(ev1seg && (timerCount < tiempoEspera)){
                    timerCount++;
                }
                else if(ev1seg && (timerCount == tiempoEspera)){
                    status = esperandoFicha;
                    func_tiempoTranscurrido();
                }
                break;

            case teElegido:
                if(ev1seg && (timerCount < tiempoTe)){
                    timerCount++;
                }
                else if(ev1seg && (timerCount == tiempoTe)){
                    func_teServido();
                    status = indicadorSonoro;
                    func_alarmaOn();
                    timerCount = 0;
                    ledStatus = 0;
                }

                if(evToggleLed && status != indicadorSonoro){
                    ledStatus = func_toggleLed(ledStatus);
                }
                break;

            case cafeElegido:
                if(ev1seg && (timerCount < tiempoCafe)){
                    timerCount++;
                }
                else if(ev1seg && (timerCount == tiempoCafe)){
                    func_cafeServido();
                    status = indicadorSonoro;
                    func_alarmaOn();
                    timerCount = 0;
                    ledStatus = 0;
                }

                if(evToggleLed && status != indicadorSonoro){
                    ledStatus = func_toggleLed(ledStatus);
                }
                break;

            case indicadorSonoro:
                if(ev1seg && (timerCount < tiempoSonoro)){
                    timerCount++;
                }
                else if(ev1seg && (timerCount == tiempoSonoro)){
                    func_alarmaOff();
                    status = esperandoFicha;
                    func_esperarFicha();
                    msHalfCycle = 500;
                }
                break;

        }
        ev1seg = 0;
        evToggleLed = 0;
        func_Pausems(1);
        msTimer++;
        if(msTimer % msHalfCycle == 0){
            evToggleLed = func_evToggleLed();
        }
        if(msTimer == 1000){
            msTimer = 0;
            ev1seg = func_ev1seg();
        }
    }
    func_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
